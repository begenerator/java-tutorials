package InterfaceImplementations;

public class Main {
    public static void main(String[] args) {

        //Classic way
        Car car = new Car();
        car.drive();

        //Lambda
        Vehicle carLambda = () -> System.out.println("Lambda Brumm ");
        carLambda.drive();

        //innerclass
        Vehicle carInnerClass = new Vehicle() {
            @Override
            public void drive() {
                System.out.println("InnerClass Brumm");
            }
        };

        carInnerClass.drive();
    }
}
