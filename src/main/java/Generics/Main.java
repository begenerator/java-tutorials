package Generics;

public class Main {
    public static void main(String[] args) {

        Lemon lemon = new Lemon();
        Berries berry = new Berries();

        Fruits citron = new Fruits(lemon);

        Fruits berryFruit = new Fruits(berry);
    }
}
